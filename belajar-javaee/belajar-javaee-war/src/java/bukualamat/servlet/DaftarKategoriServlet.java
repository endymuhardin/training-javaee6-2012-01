/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bukualamat.servlet;

import bukualamat.entity.Kategori;
import bukualamat.service.BukualamatServiceBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Student01
 */
public class DaftarKategoriServlet extends HttpServlet {
    @EJB
    private BukualamatServiceBean bukualamatServiceBean;

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String cari = request.getParameter("cari");
        
        Long jumlahKategori;
        List<Kategori> daftarKategori;
        
        if(cari != null || cari.length() > 0){
            daftarKategori = bukualamatServiceBean.cariKategoryByNama(cari);
            jumlahKategori = Long.valueOf(daftarKategori.size());
        } else {
            jumlahKategori = bukualamatServiceBean.hitungSemuaKategori();
            daftarKategori = bukualamatServiceBean.cariSemuaKategori(0, 100);
        }
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /*
             * TODO output your page here. You may use following sample code.
             */
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Daftar Kategori</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Daftar Kategori</h1>");
            
            out.println("Jumlah kategori : "+jumlahKategori);
            
            out.println("<table border=1>");
            out.println("<tr>");
            out.println("<td>Nama</td>");
            out.println("<td>&nbsp;</td>");
            out.println("</tr>");
            for (Kategori kategori : daftarKategori) {
                out.println("<tr>");
                out.println("<td>"+kategori.getNama()+"</td>");
                out.println("<td>");
                out.println("<a href=detail?id="+kategori.getId()+">Detail</a>");
                out.println("</td>");
                out.println("</tr>");
            }
            
            out.println("</table>");
            
            out.println("</body>");
            out.println("</html>");
        } finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
