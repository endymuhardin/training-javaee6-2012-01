/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bukualamat.service;

import bukualamat.entity.Kategori;
import bukualamat.entity.Kontak;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Student01
 */
@Stateless
@LocalBean
public class BukualamatServiceBean {
    @PersistenceContext(unitName = "belajar-javaee-ejbPU")
    private EntityManager em;

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    
    public void simpan(Kategori k){
        // biasanya untuk menyimpan data ke database
        // kita harus menulis SQL insert
        // dengan JPA, tidak perlu lagi
        // tinggal panggil saja method persist/merge di EntityManager
        
        if(k.getId() == null) { // id null berarti data baru
            em.persist(k);
        } else { // id tidak null, berarti data existing mau diupdate
            em.merge(k);
        }
    }
    
    public List<Kategori> cariSemuaKategori(Integer start, Integer rows){
        if(start == null || start < 0){
            start = 0;
        }
        
        if(rows == null || rows < 0){
            rows = 10;
        }
        
        List<Kategori> hasil = em.createQuery("select k from Kategori k order by k.nama")
                .setFirstResult(start)
                .setMaxResults(rows)
                .getResultList();
        return hasil;
    }
    
    public Long hitungSemuaKategori(){
        return (Long) em.createQuery("select count(k) from Kategori k")
                .getSingleResult();
    }
    
    public Kategori cariKategoriById(Long id){
        return  (Kategori) em.createQuery("select k from Kategori k where k.id = :id")
                .setParameter("id", id)
                .getSingleResult();
    }
    
    public void update(Kategori k){
        em.merge(k);
    }
    
    public void delete(Kategori k){
        em.remove(k);;
    }
    
    public List<Kategori> cariKategoryByNama(String nama){
        return em.createQuery("select k from Kategori k where lower(k.nama) like :nama order by k.nama")
                .setParameter("nama", "%"+nama+"%")
                .getResultList();
    }
    
    public void simpan(Kontak k){
        if(k.getId() == null){
            em.persist(k);
        } else {
            em.merge(k);
        }
    }
    
    public Kontak cariKontakById(Long id){
        return (Kontak) em.createQuery("select k from Kontak k where k.id = :id")
                .setParameter("id", id)
                .getSingleResult();
    }
    
    public Long hitungSemuaKontak(){
        return (Long) em.createQuery("select count(k) from Kontak k ")
                .getSingleResult();
    }
    
    public List<Kontak> cariSemuaKontak(Integer start, Integer rows){
        return  em.createQuery("select k from Kontak k order by k.nama ")
                .setFirstResult(start)
                .setMaxResults(rows)
                .getResultList();
    }
    
    public Long hitungKontakByKategori(Kategori kat){
        return (Long) em.createQuery("select count(k) from Kontak k where k.kategori.id = :id")
                .setParameter("id", kat.getId())
                .getSingleResult();
    }
    
    public List<Kontak> cariKontakByKategori(Kategori kat, Integer start, Integer rows){
        return  em.createQuery("select k from Kontak k where k.kategori.id = :id order by k.nama ")
                .setParameter("id", kat.getId())
                .setFirstResult(start)
                .setMaxResults(rows)
                .getResultList();
    }
    
    public Long hitungKontakByNamaKategori(String namaKategori){
        return (Long) em.createQuery("select count(k) from Kontak k where k.kategori.nama like :nama")
                .setParameter("nama", "%"+namaKategori+"%")
                .getSingleResult();
    }
    
    public List<Kontak> cariKontakByNamaKategori(String namaKategori, Integer start, Integer rows){
        return  em.createQuery("select k from Kontak k where k.kategori.nama like :nama order by k.nama ")
                .setParameter("nama", "%"+namaKategori+"%")
                .setFirstResult(start)
                .setMaxResults(rows)
                .getResultList();
    }

    public void persist(Object object) {
        em.persist(object);
    }
}
