/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bukualamat.jsf;

import bukualamat.entity.Kontak;
import bukualamat.service.BukualamatServiceBean;
import java.util.List;
import java.util.Locale;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;

/**
 *
 * @author Student01
 */
@ManagedBean
@SessionScoped
public class KontakManagedBean {
    @EJB
    private BukualamatServiceBean bukualamatServiceBean;
    
    private DataModel<Kontak> daftarKontak = new ListDataModel<Kontak>();
    
    private Kontak kontak = new Kontak();
    
    public Locale getLocale(){
        Locale loc = new Locale("ja", "jp");
        return loc;
    }
    
    public DataModel<Kontak> getDaftarKontak(){
        List<Kontak> semua = bukualamatServiceBean.cariSemuaKontak(0, 100);
        daftarKontak = new ListDataModel<Kontak>(semua);
        return daftarKontak;
    }
    public String simpan(){
        bukualamatServiceBean.simpan(kontak);
        kontak = new Kontak();
        return "form?faces-redirect=true";
    }

    public Kontak getKontak() {
        return kontak;
    }

    public void setKontak(Kontak kontak) {
        this.kontak = kontak;
    }
    
    /**
     * Creates a new instance of KontakManagedBean
     */
    public KontakManagedBean() {
    }
}
