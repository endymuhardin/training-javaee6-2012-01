/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bukualamat.jsf;

import bukualamat.entity.Kategori;
import bukualamat.service.BukualamatServiceBean;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;

/**
 *
 * @author Student01
 */
@ManagedBean
@SessionScoped
public class KategoriManagedBean {
    @EJB
    private BukualamatServiceBean bukualamatServiceBean;
    private DataModel<Kategori> dataModelKategori = new ListDataModel<Kategori>();
    private Kategori kategori = new Kategori();

    public Kategori getKategori() {
        return kategori;
    }

    public void setKategori(Kategori kategori) {
        this.kategori = kategori;
    }
    
    public String edit(){
        kategori = dataModelKategori.getRowData();
        System.out.println("Kategori diklik, ID:"+kategori.getId());
        return "form?faces-redirect=true";
    }

    public DataModel<Kategori> getDaftarKategori(){
        List<Kategori> daftarKategori 
                = bukualamatServiceBean.cariSemuaKategori(0, 100);
        dataModelKategori
                = new ListDataModel<Kategori>(daftarKategori);
        return dataModelKategori;
    }
    
    public String simpan(){
        System.out.println("Tombol simpan ditekan");
        
        // simpan ke database menggunakan EJB
        bukualamatServiceBean.simpan(kategori);
        
        // setelah tersimpan, reset menjadi object kosong lagi
        kategori = new Kategori();
        
        return "list?faces-redirect=true";
    }
    
    public String tambah(){
        kategori = new Kategori();
        return "form?faces-redirect=true";
    }
}
