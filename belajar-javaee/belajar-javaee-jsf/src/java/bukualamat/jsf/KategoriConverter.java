/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bukualamat.jsf;

import bukualamat.entity.Kategori;
import bukualamat.service.BukualamatServiceBean;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 *
 * @author Student01
 */
@FacesConverter(forClass=Kategori.class)
public class KategoriConverter implements Converter{

    private BukualamatServiceBean bukualamatServiceBean;
    
    public KategoriConverter(){
        try {
            InitialContext ctx = new InitialContext();
            
            // nama EJB dilihat dari log Glassfish
            String namaEjb = "java:global/belajar-javaee/belajar-javaee-ejb/BukualamatServiceBean";
            bukualamatServiceBean = (BukualamatServiceBean) ctx.lookup(namaEjb);
        } catch (NamingException ex) {
            Logger.getLogger(KategoriConverter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public Object getAsObject(FacesContext context, UIComponent component, 
                            String value) {
        Long id = Long.valueOf(value);
        return bukualamatServiceBean.cariKategoriById(id);
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        Kategori k = (Kategori) value;
        return k.getId().toString();
    }
    
}
