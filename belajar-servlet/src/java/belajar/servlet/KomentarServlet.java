/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package belajar.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Student01
 */
public class KomentarServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /*
             * TODO output your page here. You may use following sample code.
             */
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Daftar Komentar</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Komentar Pengunjung</h1>");
            
            out.println("<table>");
            out.println("<tr>");
            out.println("<td>Nama</td>");
            out.println("<td>Pesan</td>");
            out.println("</tr>");
            
            List<String[]> daftarKomentar = ambilKomentarDariDatabase();
            
            for (String[] komentar : daftarKomentar) {
                out.println("<tr>");
                out.println("<td>"+komentar[0]+"</td>");
                out.println("<td>"+komentar[1]+"</td>");
                out.println("</tr>");
            }
            
            out.println("</table>");
            
            out.println("</body>");
            out.println("</html>");
        } finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private List<String[]> ambilKomentarDariDatabase() {
        try {
            
        // 0. Variabel untuk koneksi ke database
        String dbDriver = "com.mysql.jdbc.Driver";
        String dbUrl = "jdbc:mysql://localhost/bukutamu";
        String dbUsername = "root";
        String dbPassword = "";
        
        // 1. buka koneksi ke database
        Class.forName(dbDriver); // load driver database
        Connection conn = DriverManager.getConnection(dbUrl, dbUsername, dbPassword);
        
        // 2. Buat query insert
        String sqlSelect = "select * from tbl_komentar";
        PreparedStatement psSelect = conn.prepareStatement(sqlSelect);
        
        // 3. Jalankan proses insert
        ResultSet rs = psSelect.executeQuery();
        
        // 4. Masukkan hasil query ke dalam List
        List<String[]> hasil = new ArrayList<String[]>();
        
        while(rs.next()){
            String[] komentar = new String[2];
            komentar[0] = rs.getString("nama");
            komentar[1] = rs.getString("pesan");
            hasil.add(komentar);
        }
        
        // 5. Setelah selesai, tutup koneksi ke database
        conn.close();
        
        return hasil;
        
        } catch (Exception e) {
            System.out.println("Terjadi error");
            e.printStackTrace();
            
            return null;
        }
    }
}
