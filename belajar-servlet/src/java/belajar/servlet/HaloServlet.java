/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package belajar.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Student01
 */
public class HaloServlet extends HttpServlet {

    private void simpanKomentar(String nama, String pesan) {
        System.out.println("Menyimpan pesan ["+pesan+"] dari ["+nama+"]");
        
        // simpan ke database
        try {
            
        // 0. Variabel untuk koneksi ke database
        String dbDriver = "com.mysql.jdbc.Driver";
        String dbUrl = "jdbc:mysql://localhost/bukutamu";
        String dbUsername = "root";
        String dbPassword = "";
        
        // 1. buka koneksi ke database
        Class.forName(dbDriver); // load driver database
        Connection conn = DriverManager.getConnection(dbUrl, dbUsername, dbPassword);
        
        // 2. Buat query insert
        String sqlInsert = "insert into tbl_komentar (nama, pesan) values(?,?)";
        PreparedStatement psInsert = conn.prepareStatement(sqlInsert);
        
        // 3. Isi parameter query
        psInsert.setString(1, nama);
        psInsert.setString(2, pesan);
        
        // 4. Jalankan proses insert
        psInsert.executeUpdate();
        
        // 5. Setelah selesai, tutup koneksi ke database
        conn.close();
        
        } catch (Exception e) {
            System.out.println("Terjadi error");
            e.printStackTrace();
        }
        
    }
    
    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /*
             * TODO output your page here. You may use following sample code.
             */
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Belajar Servlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet HaloServlet di URL :  " + request.getContextPath() + "</h1>");
            
            String nama = request.getParameter("nama");
            if(nama!= null){
                out.println("<b>Halo "+nama+"</b>");
                
                String pesan = request.getParameter("pesan");
                if(pesan != null){
                    simpanKomentar(nama, pesan);
                }
            }
            out.println("<form method=get>");
            out.println("Nama : <input type=text name=nama>");
            out.println("<br>Pesan <br> <textarea name=pesan rows=10 cols-40></textarea><br>");
            out.println("<input type=submit value=Proses>");
            out.println("</form>");
            out.println("</body>");
            out.println("</html>");
        } finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
